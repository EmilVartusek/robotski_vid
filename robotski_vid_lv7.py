
from os import listdir
from sklearn.metrics import mean_squared_error
from vtk.util.numpy_support import vtk_to_numpy

import cv2
import glob
import math
import numpy as np
import os
import vtk
import time
import matplotlib.pyplot as plt

"""
print("Unesite referentni model")
referent_bunny = input()
print("Unesite transformacijski model")
bunny = input()

iterations = [
    10,
    50,
    100,
    500,
    1000]

landmarks = [
    10,
    50,
    100,
    500,
    1000]

reader = vtk.vtkPLYReader()
reader.SetFileName(referent_bunny)
reader.Update()

polydata = reader.GetOutput()
points = polydata.GetPoints()
array = points.GetData()
target_numpy = vtk_to_numpy(array)

mapper = vtk.vtkPolyDataMapper()
mapper.SetInputConnection(reader.GetOutputPort())
actor1 = vtk.vtkActor()
actor1.SetMapper(mapper)

target = reader.GetOutput()

reader = vtk.vtkPLYReader()
reader.SetFileName(bunny)
reader.Update()

mapper = vtk.vtkPolyDataMapper()
mapper.SetInputConnection(reader.GetOutputPort())
actor2 = vtk.vtkActor()
actor2.SetMapper(mapper)

source = reader.GetOutput()

values_array = np.array([])
time_array = np.array([])
mse_array = np.array([])

for i in iterations:
    for l in landmarks:
        start = time.time()

        icp = vtk.vtkIterativeClosestPointTransform()
        icp.SetSource(source)
        icp.SetTarget(target)
        icp.GetLandmarkTransform().SetModeToRigidBody()
        icp.SetMaximumNumberOfIterations(i)
        icp.SetMaximumNumberOfLandmarks(l)
        icp.Update()

        icpTransformFilter = vtk.vtkTransformPolyDataFilter()
        icpTransformFilter.SetInputData(source)
        icpTransformFilter.SetTransform(icp)
        icpTransformFilter.Update()

        end = time.time()

        polydata = icpTransformFilter.GetOutput()
        points = polydata.GetPoints()
        array = points.GetData()
        icp_numpy = vtk_to_numpy(array)

        mse = mean_squared_error(target_numpy, icp_numpy)

        values_array = np.append(values_array, "i = " + str(i) +
                                    ", l = " + str(l))
        mse_array = np.append(mse_array, mse)
        time_array = np.append(time_array, end - start)

        mapper = vtk.vtkPolyDataMapper()
        mapper.SetInputConnection(icpTransformFilter.GetOutputPort())
        actor3 = vtk.vtkActor()
        actor3.SetMapper(mapper)

plt.figure()
plt.bar(values_array, mse_array)
plt.ylabel("Srednja kvadratna pogreska")
plt.xlabel("Iteracije i landmarkovi")
plt.title(bunny)
plt.xticks(rotation=90)
plt.tight_layout()

plt.figure()
plt.bar(values_array, time_array)
plt.ylabel("Vrijeme [s]")
plt.xlabel("Iteracije i landmarkovi")
plt.title(bunny)
plt.xticks(rotation=90)
plt.tight_layout()

plt.show()

"""
print("Unesite referentni model")
referent_bunny = input()
print("Unesite transformacijski model")
bunny = input()

print("Iteracije: ")
iterations = int(input())
print("Landmarkovi: ")
landmarks = int(input())

reader = vtk.vtkPLYReader()
reader.SetFileName(referent_bunny)
reader.Update()

mapper = vtk.vtkPolyDataMapper()
mapper.SetInputConnection(reader.GetOutputPort())
actor1 = vtk.vtkActor()
actor1.SetMapper(mapper)

target = reader.GetOutput()

reader = vtk.vtkPLYReader()
reader.SetFileName(bunny)
reader.Update()

mapper = vtk.vtkPolyDataMapper()
mapper.SetInputConnection(reader.GetOutputPort())
actor2 = vtk.vtkActor()
actor2.SetMapper(mapper)
actor2.GetProperty().SetColor(1.0, 0.0, 0.0)

source = reader.GetOutput()

icp = vtk.vtkIterativeClosestPointTransform()
icp.SetSource(source)
icp.SetTarget(target)
icp.GetLandmarkTransform().SetModeToRigidBody()
icp.SetMaximumNumberOfIterations(iterations)
icp.SetMaximumNumberOfLandmarks(landmarks)
icp.Update()

icpTransformFilter = vtk.vtkTransformPolyDataFilter()
icpTransformFilter.SetInputData(source)
icpTransformFilter.SetTransform(icp)
icpTransformFilter.Update()

mapper = vtk.vtkPolyDataMapper()
mapper.SetInputConnection(icpTransformFilter.GetOutputPort())
actor3 = vtk.vtkActor()
actor3.SetMapper(mapper)
actor3.GetProperty().SetColor(0.0, 1.0, 0.0)

text_actor1 = vtk.vtkTextActor()
text_actor1.SetInput(bunny)
text_actor1.SetPosition(40, 75)
text_actor1.GetTextProperty().SetFontSize(24)

#text_actor2 = vtk.vtkTextActor()
#text_actor2.SetInput("iteracija: " + str(iterations) +
#                        ", landmarks: " + str(landmarks))
#text_actor2.SetPosition(40, 40)
#text_actor2.GetTextProperty().SetFontSize(24)

renderer = vtk.vtkRenderer()
renderWindow = vtk.vtkRenderWindow()
renderWindow.SetSize(750, 750)
renderWindow.AddRenderer(renderer)
renderWindowInteractor = vtk.vtkRenderWindowInteractor()
renderWindowInteractor.SetRenderWindow(renderWindow)
renderer.AddActor(actor1)
renderer.AddActor(actor3)
renderer.AddActor2D(text_actor1)
#renderer.AddActor2D(text_actor2)
renderWindow.Render()
renderWindowInteractor.Start()