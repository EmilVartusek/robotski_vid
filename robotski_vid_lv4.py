import numpy as np
import cv2
from matplotlib import pyplot as plt

MIN_MATCH_COUNT = 10

img1 = cv2.imread('pic03.jpg', 0)    
img2 = cv2.imread('pic04.jpg', 0)

img1 = cv2.resize(img1, (0,0), fx=0.5, fy=0.5) 
img2 = cv2.resize(img2, (0,0), fx=0.5, fy=0.5) 

cv2.imshow("Slika 1", img1)
cv2.imshow("Slika 2", img2)
cv2.waitKey()
cv2.destroyAllWindows()

iz = cv2.selectROI(img1)
img1 = img1[int(iz[1]):int(iz[1]+iz[3]), int(iz[0]):int(iz[0]+iz[2])]
cv2.imshow("Objekt od interesa", img1)
cv2.waitKey()
cv2.destroyAllWindows()

sift = cv2.xfeatures2d.SIFT_create()

kp1, des1 = sift.detectAndCompute(img1,None)
kp2, des2 = sift.detectAndCompute(img2,None)

img = cv2.drawKeypoints(img1, kp1, None)
cv2.imshow("Svojstva objekta od interesa", img)
cv2.waitKey()
cv2.destroyAllWindows()

img = cv2.drawKeypoints(img2, kp2, None)
cv2.imshow("Svojstva slike na kojoj se pronalazi objekt od interesa", img)
cv2.waitKey()
cv2.destroyAllWindows()

flann = cv2.FlannBasedMatcher()

matches = flann.knnMatch(des1,des2,k=2)

good = []
for m,n in matches:
    if m.distance < 0.7*n.distance:
        good.append(m)
        
if len(good)>MIN_MATCH_COUNT:
    src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
    dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)

    M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC,5.0)
    matchesMask = mask.ravel().tolist()

    h,w = img1.shape
    pts = np.float32([ [0,0], [0,h-1], [w-1,h-1], [w-1,0] ]).reshape(-1,1,2)
    dst = cv2.perspectiveTransform(pts, M)

    img2 = cv2.polylines(img2, [np.int32(dst)], True, 255, 3, cv2.LINE_AA)
    
else:
    print "Nedovoljan broj podudaranja - %d/%d" % (len(good), MIN_MATCH_COUNT)
    matchesMask = None
    
cv2.waitKey()

draw_params = dict(matchColor = (0,255,0), 
                   singlePointColor = None,
                   matchesMask = matchesMask, 
                   flags = 2)

img3 = cv2.drawMatches(img1, kp1, img2, kp2, good, None, **draw_params)

plt.imshow(img3, 'gray'),plt.show()
