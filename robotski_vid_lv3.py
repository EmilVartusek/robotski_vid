import cv2
import glob
import math
import numpy as np
import os
import pickle


def CallBackFunc(event, x, y, flags, param):
    if event == cv2.EVENT_LBUTTONDOWN:
        coordinates.append((x, y))


def order_points(pts):
    rect = np.zeros((4, 2), dtype="float32")

    s = pts.sum(axis=1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]

    diff = np.diff(pts, axis=1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]

    return rect


def four_point_transform(image, pts):
    rect = order_points(pts)
    (tl, tr, br, bl) = rect

    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))

    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(int(heightA), int(heightB))

    dst = np.array([
        [0, 0],
        [maxWidth - 1, 0],
        [maxWidth - 1, maxHeight - 1],
        [0, maxHeight - 1]], dtype="float32")

    M = cv2.getPerspectiveTransform(rect, dst)
    warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))

    return warped

 
while(True):
    
    print("""
          1. Kalibriranje kamere (slikanje pritiskom na tipku ENTER)
          2. Slikanje slike s video kamere (slikanje pritiskom na tipku ENTER)
          3. Houghova transformacija
          4. Izlaz
          """)

    odabir_naredbe = input()
 
    if odabir_naredbe == 1:
        crit = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

        objp = np.zeros((6*8, 3), np.float32)
        objp[:, :2] = np.mgrid[0:8, 0:6].T.reshape(-1, 2)

        objpoints = []
        imgpoints = []

        for i in range(0, 10):
            cam = cv2.VideoCapture(1)
            print('Fotografija za uslikati za zavrsetak kalibracije: ' + str(10-i))
            while(True):
                ret, frame = cam.read()
                gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                cv2.imshow("Kamera", gray)

                if cv2.waitKey(1) == 13:
                    dat = "image-" + str(i+1) + ".jpg"
                    cv2.imwrite("kal_slike/" + dat, gray)
                    image = cv2.imread("kal_slike/" + dat)
                    cv2.imshow("Slika", image)
                    cv2.waitKey()
                    cv2.destroyAllWindows()
                    break

            cam.release()
            cv2.destroyAllWindows()

        print("Slikanje za kalibraciju zavrseno!")
        cam = cv2.VideoCapture(1)
        print("Slikanje testne fotografije.")
        while(True):
            ret, frame = cam.read()
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            cv2.imshow("Kamera", gray)

            if cv2.waitKey(1) == 13:
                dat = "test-image" + ".jpg"
                cv2.imwrite("kal_slike/" + dat, gray)
                image = cv2.imread("kal_slike/" + dat)
                cv2.imshow("Testna slika", image)
                cv2.waitKey()
                cv2.destroyAllWindows()
                break

        cam.release()
        cv2.destroyAllWindows()

        images = glob.glob("kal_slike/image-*.jpg")
        for fname in images:
            img = cv2.imread(fname)
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            ret, corners = cv2.findChessboardCorners(gray, (8, 6), None)
            if ret:
                objpoints.append(objp)
                imgpoints.append(corners)
                img = cv2.drawChessboardCorners(img, (8, 6), corners, ret)
                img = cv2.drawChessboardCorners(img, (8, 6), corners, ret)
                cv2.imshow("Slika", img)
                cv2.waitKey()
                cv2.destroyAllWindows()

        img = cv2.imread("kal_slike/test-image.jpg")
        img_size = (img.shape[1], img.shape[0])

        ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints,
                                                           imgpoints,
                                                           img_size,
                                                           None,
                                                           None)
        dst = cv2.undistort(img, mtx, dist, None, mtx)

        cv2.imshow("Originalna slika", img)
        cv2.waitKey()
        cv2.destroyAllWindows()

        cv2.imshow("Ispravljena slika", dst)
        cv2.waitKey()
        cv2.destroyAllWindows()

        dist_pickle = {}
        dist_pickle["mtx"] = mtx
        dist_pickle["dist"] = dist
        pickle.dump(dist_pickle, open("par_kal.p", "wb"))

    elif odabir_naredbe == 2:
        
        cam = cv2.VideoCapture(1)

        while(True):
            ret, frame = cam.read()
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            cv2.imshow('frame', gray)

            if cv2.waitKey(1) == 13:
                dat = 'cam_slika.png'
                cv2.imwrite(dat, gray)
                img = cv2.imread(dat)
                cv2.imshow('Slika - uslikana kamerom', img)
                cv2.waitKey()
                break

        cam.release()
        cv2.destroyAllWindows()

    elif odabir_naredbe == 3:
        if os.path.isfile('./par_kal.p') is not True:
            print("Kamera nije kalibrirana. Kalibrirajte kameru.")
            continue

        pickle_in = open("par_kal.p", "rb")
        example_dict = pickle.load(pickle_in)
        mtx = example_dict['mtx']
        dist = example_dict['dist']

        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        img = cv2.undistort(img, mtx, dist, None, mtx)
        cv2.imshow('Ispravljena slika', img)
        cv2.waitKey()
        cv2.destroyAllWindows()

        windowName = 'Odabir 4 tocke misem'
        cv2.namedWindow(windowName)
        coordinates = []
        cv2.setMouseCallback(windowName, CallBackFunc)

        cv2.imshow(windowName, img)
        cv2.waitKey()
        cv2.destroyAllWindows()

        pts = np.array(eval(str(coordinates)), dtype="float32")
        img = four_point_transform(img, pts)

        cv2.imshow("Slika", img)
        cv2.waitKey()
        cv2.destroyAllWindows()

        edges = cv2.Canny(img, 100, 100)
        cv2.imshow('Detekcija rubova Canny metodom', edges)
        cv2.waitKey()
        cv2.destroyAllWindows()

        img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)

        lines = cv2.HoughLines(edges, 1, np.pi/180, 50)
        for rho, theta in lines[0]:
            a = np.cos(theta)
            b = np.sin(theta)
            x0 = a*rho
            y0 = b*rho
            x1 = int(x0 + 1000*(-b))
            y1 = int(y0 + 1000*(a))
            x2 = int(x0 - 1000*(-b))
            y2 = int(y0 - 1000*(a))

            cv2.line(img, (x1, y1), (x2, y2), (0, 0, 255), 2)

        pixels = lines[0][0][0]
        w_img = img.shape[1]
        scale = w_img / 200
        mms = pixels / scale

        rads = lines[0][0][1]
        degs = rads * 180 / np.pi

        angle = rads
        length = pixels
        x1 = 0
        y1 = 0
        x2 = int(x1 + length * math.cos(angle))
        y2 = int(y1 + length * math.sin(angle))

        cv2.line(img, (x1, y1), (x2, y2), (0, 255, 0), 2)

        font = cv2.FONT_HERSHEY_SIMPLEX
        topLeftCornerOfText = (50, 50)
        fontScale = 0.5
        fontColor = (0, 0, 0)
        lineType = 2

        cv2.putText(img,
                    str(str(round(mms, 2)) + " mm, " +
                        str(round(degs, 2)) + " deg"),
                    topLeftCornerOfText,
                    font,
                    fontScale,
                    fontColor,
                    lineType)

        cv2.imshow('Konacno rjesenje', img)
        cv2.waitKey()
        cv2.destroyAllWindows()

    elif odabir_naredbe == 4:
        break

    else:
        print("Krivi odabir naredbe! Odaberite ponovno.")