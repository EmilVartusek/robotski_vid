import cv2
import numpy as np
#from matplotlib import pyplot as plt

while(True):
    
    print("""
          1. Spremanje slike s video kamere (slikanje pritiskom na tipku ENTER)
          2. Ucitavanje slike (upisati naziv slike koja se nalazi u direktoriju)
          3. Izrezi sliku (lijevom tipkom misa)
          4. Procesiraj sliku (Canny)
          5. Template matching
          6. Izlaz
          """)

    odabir_naredbe = input()

    if odabir_naredbe == 1:
        
        cam = cv2.VideoCapture(0)

        while(True):
            ret, frame = cam.read()
            sivi_tonovi = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            cv2.imshow('frame', sivi_tonovi)

            if cv2.waitKey(1) == 13:
                dat = 'cam_slika.png'
                cv2.imwrite(dat, sivi_tonovi)
                slika = cv2.imread(dat)
                cv2.imshow('Slika - uslikana kamerom', slika)
                cv2.waitKey()
                break

        cam.release()
        cv2.destroyAllWindows()
           
    elif odabir_naredbe == 2:
        dat = raw_input()
        slika = cv2.imread(dat)
        cv2.imshow("Slika - ucitana", slika)
        cv2.waitKey()
        cv2.destroyAllWindows()
        
    elif odabir_naredbe == 3:
        iz = cv2.selectROI(slika)
        iz_slika = slika[int(iz[1]):int(iz[1]+iz[3]), int(iz[0]):int(iz[0]+iz[2])]
        cv2.imshow("Izrezana slika", iz_slika)
        #slika = iz_slika
        cv2.waitKey()
        cv2.destroyAllWindows()
        
    elif odabir_naredbe == 4:
        rubovi = cv2.Canny(slika, 100, 200)
        cv2.imshow('Slika nakon upotrebe Canny-a', rubovi)
        cv2.waitKey()
        cv2.destroyAllWindows()
        
    elif odabir_naredbe == 5:
        tmp_slika = slika
        #img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)
        tmp_uzorak = iz_slika
        w, h = tmp_uzorak.shape[:-1]

        rez = cv2.matchTemplate(tmp_slika, tmp_uzorak, cv2.TM_CCOEFF_NORMED)
        threshold = 0.8
        loc = np.where( rez >= threshold)
        for pt in zip(*loc[::-1]):
            cv2.rectangle(tmp_slika, pt, (pt[0] + w, pt[1] + h), (0,0,255), 2)

        #cv2.imwrite('rez.png',tmp_slika)
        cv2.imshow('Template matching', tmp_slika)
        cv2.waitKey()
        cv2.destroyAllWindows()
        
    elif odabir_naredbe == 6:
        break
    
    else:
        print("Krivi odabir naredbe! Odaberite ponovno.")

