import cv2
import glob
import math
import numpy as np
import os
from os import listdir
import pickle
import plotly.graph_objs as go
from plotly.offline import init_notebook_mode, plot


def Convert2DPointsTo3DPoints(pts2D_L, pts2D_R, E, P):
    P = np.matrix(P)
    D, U, Vt = cv2.SVDecomp(E)

    W = [[0, -1, 0],
         [1, 0, 0],
         [0, 0, 1]]
    W = np.float32(W)

    A = np.matrix(U.dot(W).dot(Vt))

    b = np.zeros([3, 1], np.float)
    b[:, 0] = U[:, 2]

    Ainv_b = A.I.dot(b)

    Lpi = np.zeros([3, 1], np.float)
    Rpi = np.zeros([3, 1], np.float)
    ARpi = np.zeros([3, 1], np.float)

    S = np.zeros([2, 1], np.float)

    X = np.zeros([2, 2], np.float)
    x1 = np.zeros([1, 1], np.float)
    x2 = np.zeros([1, 1], np.float)
    x4 = np.zeros([1, 1], np.float)
    Y = np.zeros([2, 1], np.float)

    y1, y2 = np.zeros([1, 1], np.float), np.zeros([1, 1], np.float)

    Lm, Rm = np.zeros([3, 1], np.float), np.zeros([3, 1], np.float)

    pts3D = []

    for i in range(len(pts2D_L)):
        Lm[0, 0] = pts2D_L[i][0]
        Lm[1, 0] = pts2D_L[i][1]
        Lm[2, 0] = 1

        Rm[0, 0] = pts2D_R[i][0]
        Rm[1, 0] = pts2D_R[i][1]
        Rm[2, 0] = 1

        Lpi = P.I.dot(Lm)
        Rpi = P.I.dot(Rm)

        # Init X table
        ARpi = A.I.dot(Rpi)
        x1 = Lpi.T.dot(Lpi)
        x2 = Lpi.T.dot(ARpi)
        x4 = ARpi.T.dot(ARpi)

        X[0, 0] = -x1[0, 0]
        X[0, 1] = x2[0, 0]
        X[1, 0] = x2[0, 0]
        X[1, 1] = -x4[0, 0]

        # Init Y table
        y1 = Lpi.T.dot(Ainv_b)
        y2 = ARpi.T.dot(Ainv_b)
        Y[0, 0] = -y1[0, 0]
        Y[1, 0] = y2[0, 0]

        cv2.solve(X, Y, S)

        s = S[0, 0]
        t = S[1, 0]

        Lpi = s*Lpi
        ARpi = t*ARpi

        pts3D.append(np.array(Lpi + ARpi - Ainv_b/2).T)

    return np.array(pts3D).reshape(len(pts3D), 3)

while(True):
    
    print("""
          1. Uslikaj 1. sliku (slikanje pritiskom na tipku ENTER)
          2. Uslikaj 2. sliku (slikanje pritiskom na tipku ENTER)
          3. Trodimenzionalna rekonstrukcija scene iz 2 slike
          4. Izlaz
          """)

    odabir_naredbe = raw_input()
 
    if odabir_naredbe == "1":
        
        cam = cv2.VideoCapture(0)

        while(True):
            ret, frame = cam.read()
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            cv2.imshow('frame', gray)

            if cv2.waitKey(1) == 13:
                dat = 'img1_lv5.png'
                cv2.imwrite(dat, gray)
                slika = cv2.imread(dat)
                cv2.imshow('Slika - uslikana kamerom', slika)
                cv2.waitKey()
                break

        cam.release()
        cv2.destroyAllWindows()

    elif odabir_naredbe == "2":
        
        cam = cv2.VideoCapture(0)

        while(True):
            ret, frame = cam.read()
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            cv2.imshow('frame', gray)

            if cv2.waitKey(1) == 13:
                dat = 'img2_lv5.png'
                cv2.imwrite(dat, gray)
                slika = cv2.imread(dat)
                cv2.imshow('Slika - uslikana kamerom', slika)
                cv2.waitKey()
                break

        cam.release()
        cv2.destroyAllWindows()    

    elif odabir_naredbe == "3":

        img1 = cv2.imread('img1_lv5.png', 0)    
        img2 = cv2.imread('img2_lv5.png', 0)


        cv2.imshow("Slika 1", img1)
        cv2.imshow("Slika 2", img2)
        cv2.waitKey()
        cv2.destroyAllWindows()

        sift = cv2.xfeatures2d.SIFT_create()

        kp1, des1 = sift.detectAndCompute(img1, None)
        kp2, des2 = sift.detectAndCompute(img2, None)

        flann = cv2.FlannBasedMatcher()
        matches = flann.knnMatch(des1, des2, k=2)

        good = []
        pts1 = []
        pts2 = []

        for m, n in matches:
            if m.distance < 0.75*n.distance:
                good.append(m)
                pts1.append(kp1[m.queryIdx].pt)
                pts2.append(kp2[m.trainIdx].pt)

        pts1 = np.int32(pts1)
        pts2 = np.int32(pts2)

        img3 = cv2.drawMatches(img1, kp1, img2, kp2, good, None, flags=2)
        cv2.imshow("Sve sparene znacajke", img3)
        cv2.waitKey()
        cv2.destroyAllWindows()

        F, mask = cv2.findFundamentalMat(pts1, pts2, cv2.FM_RANSAC)

        ep_pts1 = pts1[mask.ravel() == 1]
        ep_pts2 = pts2[mask.ravel() == 1]
        ep_pts_len = len(ep_pts1)

        best = []
        for m in good:
            bpts1 = np.int32(kp1[m.queryIdx].pt)
            bpts2 = np.int32(kp2[m.trainIdx].pt)
            for i in range(ep_pts_len):
                if (np.array_equal(bpts1, ep_pts1[i]) and
                    np.array_equal(bpts2, ep_pts2[i])):
                    best.append(m)

        img4 = cv2.drawMatches(img1, kp1, img2, kp2, best, None, flags=2)
        cv2.imshow("Dobro sparene znacajke", img4)
        cv2.waitKey()
        cv2.destroyAllWindows()

        pickle_in = open("par_kal.p", "rb")
        example_dict = pickle.load(pickle_in)
        mtx = example_dict['mtx']

        mtx = np.array(mtx)
        E, mask = cv2.findEssentialMat(ep_pts1, ep_pts2, mtx, cv2.RANSAC)

        pts3D = Convert2DPointsTo3DPoints(ep_pts1, ep_pts2, E, mtx)

        x = pts3D[:, 0]
        y = pts3D[:, 1]
        z = pts3D[:, 2]

        data = [
            go.Mesh3d(x=x,
                        y=y,
                        z=z,
                        opacity=0.8,
                        color='#00FFFF')
        ]

        layout = go.Layout(
            autosize=False,
            width=800,
            height=800
        )

        fig = go.Figure(data=data, layout=layout)

        camera = dict(
            up=dict(x=0, y=0, z=1),
            center=dict(x=0, y=0, z=0),
            eye=dict(x=1.9, y=-1.3, z=1)
        )

        fig['layout'].update(
            scene=dict(camera=camera)
        )

        plot(fig, validate=False)

    elif odabir_naredbe == "4":
        break
    
    else:
        print("Krivi odabir naredbe! Odaberite ponovno.")
