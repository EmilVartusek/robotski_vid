import cv2
import numpy as np
from os import listdir
import matplotlib.pyplot as plt

def ReadKinectPics():
    KinectPics = []
    RGBPics = []
    pics_path = "./kin_slike/"
    files = [f for f in listdir(pics_path)]
    files = np.sort(files)

    for i in range(0, len(files), 2):
        D = np.loadtxt(pics_path+files[i])
        RGB = cv2.imread(pics_path+files[i+1])
        RGBPics.append(RGB)
        pts3D = []
        dmin = 2047
        dmax = 0
        for v in range(D.shape[0]):
            for u in range(D.shape[1]):
                if D[v, u] == 2047:
                    D[v, u] = -1
                elif D[v, u] < dmin:
                    dmin = D[v, u]
                elif D[v, u] > dmax:
                    dmax = D[v, u]
                if D[v, u] != -1:
                    pts3D.append([v, u, D[v, u]])
        D = (D-dmin)*254/(dmax-dmin)+1
        D[D < 0] = 0
        D = np.uint8(D)
        KinectPics.append([np.array(pts3D), D])

    return KinectPics, RGBPics

def get_plane_from_random_points():
    random_points_indexes = np.random.choice(points_len, 3, replace=False)
    random_points = points[random_points_indexes]

    u = random_points[:, 0]
    v = random_points[:, 1]
    d = random_points[:, 2]
    mat = np.array([[u[0], v[0], 1],
                    [u[1], v[1], 1],
                    [u[2], v[2], 1]])

    try:
        return np.matmul(np.linalg.inv(mat), d.T)
    except np.linalg.LinAlgError as err:
        if 'Singular matrix' in str(err):
            print("Singularna matrica, pokusajte opet.")
        return []


def get_points_on_plane(plane, threshold):
    a = plane[0]
    b = plane[1]
    c = plane[2]
    d = points[:, 2]
    u = points[:, 0]
    v = points[:, 1]

    tmp_points = np.abs(d - (a * u + b * v + c))
    plane_points_indexes = np.where(tmp_points < threshold)[0]
    return points[plane_points_indexes]


KPics, RGBPics = ReadKinectPics()

for img_index in range(0, 9):
    points = np.int32(KPics[img_index][0])
    depth_map = KPics[img_index][1]
    depth_map = cv2.cvtColor(depth_map, cv2.COLOR_GRAY2BGR)

    points_len = points.shape[0]
    iterations = 1000
    threshold = 3

    r_dominant = np.array([])
    t_dominant = np.array([])

    for i in range(iterations):
        r = []

        while(r == []):
            r = get_plane_from_random_points()

        tmp_points = get_points_on_plane(r, threshold)
        if tmp_points.shape[0] > t_dominant.shape[0]:
            t_dominant = tmp_points
            r_dominant = r

    depth_map_indexes = t_dominant[:, :2]
    depth_map[depth_map_indexes[:, 0], depth_map_indexes[:, 1]] = [255, 0, 0]

    rgb_img = RGBPics[img_index]
    image = np.hstack((rgb_img, depth_map))
    cv2.imshow('Rjesenje slike ' , image)
    cv2.waitKey()
    cv2.destroyAllWindows()